import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PageLayoutComponent } from './page-layout/page-layout.component';
import { FillerComponent } from './filler/filler.component';
import { GridPageLayoutComponent } from './grid-page-layout/grid-page-layout.component';

@NgModule({
    declarations: [AppComponent, PageLayoutComponent, FillerComponent, GridPageLayoutComponent],
    imports: [BrowserModule, AppRoutingModule],
    providers: [],
    bootstrap: [AppComponent],
})
export class AppModule {}
