import { Component, OnInit } from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';

@Component({
    selector: 'app-page-layout',
    templateUrl: './page-layout.component.html',
    styleUrls: ['./page-layout.component.scss'],
})
export class PageLayoutComponent implements OnInit {
    public isPortrait = false;
    public isVerySmallScreen = false;

    constructor(private _observer: BreakpointObserver) {
        this._observer
            .observe('screen and (orientation: portrait)')
            .subscribe((event) => {
                this.isPortrait = event.matches;
            });

        this._observer
            .observe('screen and (max-width: 500px)')
            .subscribe((event) => {
                this.isVerySmallScreen = event.matches;
            });
    }

    ngOnInit(): void {}
}
